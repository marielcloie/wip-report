
$(elements.start_range).calendar({
    type: 'date',
    onChange: function(date, text, mode) {
        console.log(date);
        $(elements.end_range).calendar({
            type: 'date',
            minDate: date
        })
    }
});
$(elements.end_range).calendar({
    type: 'date',
    onChange: function(date, text, mode) {
        console.log(date);
        $(elements.start_range).calendar({
            type: 'date',
            maxDate: date
        })
    }
});

$(document).on("change", elements.slc_date_type, function(){
    let val = $(this).val();


    if(val === "Specific"){
        $(elements.cont_range).hide();
        $(elements.cont_specific).show();
    }else{
        $(elements.cont_specific).hide();
        $(elements.cont_range).show();
    }
})


$(document).on("click", elements.btn_generate_wip, function(){

    generate_template(DealsRecords, "#tbl-summary-report" , "summary-list-script", function(){
        fetch_deals(DealsRecords);
    });
})

let post_records = [];


function fetch_deals(id, index = 0){
    let current_deal = id[index];
    let deal_Record = {};
    let new_Record = {};
    let current_row = $("." + current_deal);
    console.log(current_row);
    current_row.find('.td-status').html("Current running, please wait...");
    //quote
    let estimated_sent_st = 0;
    let estimated_sent_gt = 0;
    let estimated_approved_st = 0;
    let estimated_approved_gt = 0;
    let estimated_declined_st = 0;
    let estimated_declined_gt = 0;

    //sales order
    let so_active_st = 0;
    let so_active_gt = 0;
    let so_cancelled_st = 0;
    let so_cancelled_gt = 0;

    //invoice
    let inv_active_st = 0;
    let inv_active_gt = 0;
    let inv_voided_st = 0;
    let inv_voided_gt = 0;

    //purchase
    let purchase_create = 0;
    let purchase_received = 0;
    let purchase_paid = 0;

    let val = $(elements.slc_date_type).val();
    let date_start = "";
    let date_end = "";
    if(val=== "Specific"){
        new_Record.Start_Date = null;
        new_Record.End_Date = moment($(elements.inp_wip_date).val()).format('YYYY-MM-DD');
        date_start = new Date("2000-01-01").getTime();
        date_end =  new Date(moment($(elements.inp_wip_date).val()).format('YYYY-MM-DD')).getTime();
    }else{
        new_Record.Start_Date = moment($(elements.inp_start_date).val()).format('YYYY-MM-DD');
        new_Record.End_Date = moment($(elements.inp_end_date).val()).format('YYYY-MM-DD');
        date_start = new Date(moment($(elements.inp_start_date).val()).format('YYYY-MM-DD')).getTime();
        date_end = new Date(moment($(elements.inp_end_date).val()).format('YYYY-MM-DD')).getTime();
    }

    return sub_Main.get_individual_record("Deals", current_deal ).then(function(data){
        current_row.find('.td-status').html("Record fetched, please wait...");

        deal_Record = data;
        new_Record.Name = data.Deal_Name;
        new_Record.Job = data.id;
        current_row.find('.td-deal-name').html(data.Deal_Name);
        return sub_Main.zcrm_getRelatedRecords("Deals", current_deal, "Quotes");

    }).then(function(data){
        current_row.find('.td-status').html("Quotes fetched, please wait...");

        _.forEach(data, function(o){

            let date_submitted = new Date(o.Date_Submitted).getTime();
            let date_accepted_declined = new Date(o.Date_Accepted_Declined).getTime();
            if(o.Quote_Stage === "Submitted" &&  (date_submitted >= date_start && date_submitted <= date_end)){
                estimated_sent_st += check_value(o.quotecalculation__Total_Including_Discount);
                estimated_sent_gt += check_value(o.quotecalculation__Total_Including_Tax);
            }
            else if(o.Quote_Stage === "Accepted" &&  (date_accepted_declined >= date_start && date_accepted_declined <= date_end)){
                estimated_approved_st += check_value(o.quotecalculation__Total_Including_Discount);
                estimated_approved_gt += check_value(o.quotecalculation__Total_Including_Tax);
            }
            else if((o.Quote_Stage === "Cancelled" || o.Quote_Stage === "Declined") &&  (date_accepted_declined >= date_start && date_accepted_declined <= date_end)){
                estimated_declined_st += check_value(o.quotecalculation__Total_Including_Discount);
                estimated_declined_gt += check_value(o.quotecalculation__Total_Including_Tax);
            }
        })
        new_Record.Quote_Submitted_Sub_Total = roundFixed(estimated_sent_st);
        new_Record.Quote_Submitted_Grand_Total = roundFixed(estimated_sent_gt);
        new_Record.Quote_Approved_Sub_Total = roundFixed(estimated_approved_st);
        new_Record.Quote_Approved_Grand_Total = roundFixed(estimated_approved_gt);
        new_Record.Quote_Declined_Sub_Total = roundFixed(estimated_declined_st);
        new_Record.Quote_Declined_Grand_Total = roundFixed(estimated_declined_gt);


        return sub_Main.zcrm_getRelatedRecords("Deals", current_deal, "SalesOrders");
    }).then(function(data){
        current_row.find('.td-status').html("Sales Order fetched, please wait...");

        data = _.filter(data, ['Include_on_WIP', true]);

        _.forEach(data, function(o){
            let date_created = new Date(o.Contract_Created).getTime();
            let date_cancelled = new Date(o.Date_Cancelled).getTime();
            if(o.Status === "Cancelled" &&  (date_cancelled >= date_start && date_cancelled <= date_end)){
                so_cancelled_st += check_value(o.quotecalculation__Total_Including_Discount);
                so_cancelled_gt += check_value(o.quotecalculation__Total_Including_Tax);
            }else if(date_created >= date_start && date_created <= date_end){
                so_active_st += check_value(o.quotecalculation__Total_Including_Discount);
                so_active_gt += check_value(o.quotecalculation__Total_Including_Tax);
            }
        })

        new_Record.Sales_Order_Sub_Total = roundFixed(so_active_st);
        new_Record.Sales_Order_Grand_Total = roundFixed(so_active_gt);
        new_Record.Cancelled_Sales_Order_Grand_Total = roundFixed(so_cancelled_st);
        new_Record.Cancelled_Sales_Order_Sub_Total = roundFixed(so_cancelled_gt);

        return sub_Main.zcrm_getRelatedRecords("Deals", current_deal, "Related_Invoices");
    }).then(function(data){
        current_row.find('.td-status').html("Invoices fetched, please wait...");
        data = _.filter(data, ['Include_on_WIP', true]);
        _.forEach(data, function(o){
            let date_invoiced = new Date(o.Invoice_Date).getTime();
            let date_voided = new Date(o.Date_Voided).getTime();
            if(o.Status === "Voided" &&  (date_voided >= date_start && date_voided <= date_end)){
                inv_voided_st += check_value(o.quotecalculation__Total_Including_Discount);
                inv_voided_gt += check_value(o.quotecalculation__Total_Including_Tax);
            }else if(date_invoiced >= date_start && date_invoiced <= date_end){
                inv_active_st += check_value(o.quotecalculation__Total_Including_Discount);
                inv_active_gt += check_value(o.quotecalculation__Total_Including_Tax);
            }
        })
        new_Record.Invoice_Sub_Total = roundFixed(inv_active_st);
        new_Record.Invoice_Grand_Total = roundFixed(inv_voided_gt);
        new_Record.Voided_Invoice_Sub_Total = roundFixed(inv_voided_st);
        new_Record.Voided_Invoice_Grand_Total = roundFixed(inv_voided_gt);

        return sub_Main.createRecords("WIP", [new_Record])
    }).then(function(data){
        current_row.find('.td-status').html("Completed");
        index++;
        if(id[index] === undefined){
            dynamic_modal_display("Success", id.length + " reports generated at WIP module.");
            console.log("Completed")
        }else{
            setTimeout(function(){
                fetch_deals(id, index)
            }, 1000)

        }
    })
}
Handlebars.registerHelper('addOne', function(index) {
    console.log(index);
    return index+1
});


Handlebars.registerHelper('getURL', function(api_name, id) {
    let module = _.find(module_list, ['api_name', api_name ]);
    return base_link + "/tab/" + module.module_name + "/" + id;
});

Handlebars.registerHelper('fixAmount', function(amount) {
    if (amount!==null){
        return commaSeparateNumber(check_value(amount).toFixed(2));
    }else{
        return "0.00";
    }
});

Handlebars.registerHelper('getModuleName', function(module_name, label_type, case_type) {
    let find_mod = _.find(module_list, ['api_name', module_name]);
    return (case_type === "capital") ? find_mod[label_type].toUpperCase() : find_mod[label_type];
});

Handlebars.registerHelper('dateHelper', function(date_rec) {
    if(date_rec===null){
        return "";
    }else{
        return moment.parseZone(date_rec).format("DD-MM-YYYY");
    }
});

Handlebars.registerHelper('timeHelper', function(date_rec) {
    if(date_rec===null){
        return "";
    }else{
        return moment.parseZone(date_rec).format("DD/MM/YYYY hh:mm A");
    }
});


Handlebars.registerHelper('subOneTotal', function(context , field_name, type) {
    let totalamount = 0;
    _.forEach(context ,function(value){
        totalamount = totalamount + value[field_name];
    });
    if(type === 'currency'){
        return check_negative(totalamount);
    }else{
        return totalamount.toFixed(2);
    }
});
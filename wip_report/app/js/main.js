let DealsRecords = [];

let module_list= [];
let base_link;
let org_link = "";
let ZOHOAPIURL;
let org_result;

let sub_Main = new subMain();

ZOHO.embeddedApp.on("PageLoad",function(data) {
    console.log(data);
    DealsRecords = data.EntityId;
    $(elements.span_selection).html("You have selected " + DealsRecords.length + " records to generate report");
    sub_Main.get_variable(varData).then(function (resp) {
        ZOHOAPIURL = resp.Success.Content.ZOHOAPIURL.value;
        org_link = resp.Success.Content.CRM_Base_URL.value;
        return sub_Main.get_org_info();
    }).then(function (data) {
        org_result = data;
        base_link = org_link + "/crm/org" + org_result.zgid;
        return sub_Main.get_module_info()
    }).then(function(data){
        module_list = data;

        let current_date = new Date(moment().year(),moment().get('month'),moment().get('date'));
        $('.input-date').calendar({
            type: 'date',
            initialDate: current_date,
            monthFirst : false,
        });

    }).catch((err) => {
        console.log(err);

    });
});

ZOHO.embeddedApp.init().then(function(){
    ZOHO.CRM.UI.Resize({height:"500",width:"1000"}).then(function(){

    })
});

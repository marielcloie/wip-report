
let elements = {
    span_selection                 : "#span-selection",
    btn_generate_wip               : "#btn-generate-wip",

    slc_date_type                  : "#slc-date-type",
    cont_specific                  : "#cont-specific",
    cont_range                     : "#cont-range",
    start_range                    : "#range-start",
    end_range                      : "#range-end",
    inp_start_date                 : "#record-start-date",
    inp_end_date                   : "#record-end-date",
    inp_wip_date                   : "#wip-end-date",
}
let loader_elements = {
    dynamic_modal_container                            : "#dynamic-modal",
    dynamic_modal_header                               : "#modal-header-title",
    dynamic_modal_body                                 : "#modal-body-msg",
};

let varData = {apiKeys: ["CRM_Base_URL", "ZOHOAPIURL"]};

function dynamic_modal_display(header, body){
    $(loader_elements.dynamic_modal_header).html(header);
    $(loader_elements.dynamic_modal_body).html(body);
    $(loader_elements.dynamic_modal_container).modal('show');
}


function progress_bar_init(html_temp, label_temp, percent){
    $(html_temp).css('width', percent);
    $(label_temp).text(percent);
}

function check_value(value){
    if(value === "" || value===undefined || value===null){
        value= 0;
    }else{
        value = value.toString().replace(/[^0-9.-]/g, "");
    }

    value = parseFloat(value);
    return isNaN(value)? 0.00 : value ;
}

function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
}


function generate_template(template_array, template_html, template_script, callback){
    let templatesrc = $("#" + template_script).html();
    let template = Handlebars.compile(templatesrc);
    let html = template(template_array);
    $(template_html).html(html);
    if(callback){
        callback();
    }
}

function append_template(template_array, template_html, template_script, callback){
    let templatesrc = $("#" + template_script).html();
    let template = Handlebars.compile(templatesrc);
    let html = template(template_array);
    $(template_html).append(html);
    if(callback){
        callback();
    }
}


function roundFixed(number, decimals = 2) {
    let x = Math.pow(10, Number(decimals) + 1);
    return (Number(number) + (1 / x)).toFixed(decimals)
}


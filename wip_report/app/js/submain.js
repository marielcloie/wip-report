class subMain {
    constructor(props) {
        this.props = props;
    }

    get_module_info(){
        return ZOHO.CRM.META.getModules().then(function(module){
            return Promise.resolve(module.modules);
        });
    }

    get_module_field_list(modulename){
        return ZOHO.CRM.META.getFields({"Entity":modulename}).then(function(data){
            return Promise.resolve(data.fields);
        });
    }

    get_variable(variable_name){
        return ZOHO.CRM.API.getOrgVariable(variable_name).then(function(res){
            return res;
        });

    }

    get_org_info(){
        return ZOHO.CRM.CONFIG.getOrgInfo().then(function(data){
            return Promise.resolve(data.org[0]);
        });
    }

    get_individual_record(module, id){
        return ZOHO.CRM.API.getRecord({Entity:module,RecordID:id})
            .then(function(data){
                return data.data[0];
            })
    }

    getAllRecords(modulename, pageNo = 1, tempRecords = []) {
        let self = this;
        return ZOHO.CRM.API.getAllRecords({Entity: modulename, sort_order: "asc", page: pageNo}).then((resp) => {
            if (resp.status !== 204) {
                tempRecords = tempRecords.concat(resp.data);
                if (resp.info.more_records === true) {
                    return self.getAllRecords(modulename, ++pageNo, tempRecords);
                } else {
                    return Promise.resolve(tempRecords);
                }
            } else {
                return Promise.resolve(tempRecords);
            }
        });
    }


    zcrm_getRelatedRecords(modulename, recordID, relatedModuleName, pageNo = 1, tempRecords = []) {
        let self = this;
        return ZOHO.CRM.API.getRelatedRecords({
            Entity: modulename,
            RecordID: recordID,
            RelatedList: relatedModuleName,
            sort_order: "asc",
            page: pageNo
        }).then((resp) => {
            if (resp.status !== 204) {
                tempRecords = tempRecords.concat(resp.data);
                if (resp.info.more_records === true) {
                    return self.zcrm_getRelatedRecords(modulename, recordID, relatedModuleName, ++pageNo, tempRecords);
                } else {
                    return Promise.resolve(tempRecords);
                }
            } else {
                return Promise.resolve(tempRecords);
            }
        });
    }


    CRM_searchRecords(modulename, query, page, tempRecords = []){
        let self = this;
        return ZOHO.CRM.API.searchRecord({Entity:modulename,Type:"criteria",Query:"query"}, page)
            .then(function(resp){
                if(resp.status!==204){
                    tempRecords = tempRecords.concat(resp.data);
                    if(resp.info.more_records===true){
                        page++;
                        return self.CRM_searchRecords(modulename, query, page, tempRecords);
                    }
                    else{
                        return Promise.resolve(tempRecords);
                    }
                }else{
                    return Promise.resolve(tempRecords);
                }
            })
    }

    zcrm_invokeCall(query_param, limit = 200 , offset = 0, tempRecords = []){
        console.log(query_param)
        let self = this;
        let conn_name = "widget_connection";
        let req_data ={
            "parameters" : {
                "select_query" : query_param + " LIMIT " + offset +  ", " + limit,
            },
            "method" : "POST",
            "url" : ZOHOAPIURL + "/crm/v2.1/coql",
        };
        return ZOHO.CRM.CONNECTION.invoke(conn_name, req_data)
            .then(function(data){
                console.log(data);
                let resp = (data.details===undefined)? data.statusMessage : data.details.statusMessage;
                if(resp===""){
                    return Promise.resolve(tempRecords);
                }else if(data.details.statusMessage === "Invoke URL API Execution Limit reached"){
                    return Promise.resolve(tempRecords);
                }else{
                    tempRecords = tempRecords.concat(resp.data);
                    if(resp.info.more_records){
                        return self.zcrm_invokeCall(query_param, limit , (offset+200), tempRecords);
                    }else{
                        return Promise.resolve(tempRecords);
                    }
                }
            });
    }

    getQueryCOQL(select_query, offset = 0, tempRecords = []){
        let self = this;
        let params =  {
            "select_query" : `${select_query} LIMIT ${offset}, 200`,
        }

        return ZOHO.CRM.API.coql(params).then((data) => {
            data = data.details !== undefined ? data.details : data;
            if(data.data !== undefined){
                tempRecords = tempRecords.concat(data.data);
                if(data.info.more_records===true){
                    return self.getQueryCOQL(select_query, offset + 200, tempRecords);
                }
                else{
                    return Promise.resolve(tempRecords);
                }
            }else{
                return Promise.resolve(tempRecords);
            }
        });
    }


    fetch_multiple_ids(id_list, module, index = 0, tempRecords = []){
        let self= this;
        let conn_name = "widget_connection";
        let req_data ={
            "url" : ZOHOAPIURL +"/crm/v2/" + module + "?ids=" + id_list[index],
            "method" : "GET",
        };
        return ZOHO.CRM.CONNECTION.invoke(conn_name, req_data)
            .then(function(data) {
                index++;
                console.log(data);
                let resp = (data.details===undefined)? data.statusMessage : data.details.statusMessage;
                if(resp===""){
                    return Promise.resolve(tempRecords);
                }else{
                    tempRecords = tempRecords.concat(resp.data);
                    if(id_list[index] === undefined){
                        return Promise.resolve(tempRecords);
                    }else{
                        return self.fetch_multiple_ids(id_list , module, index, tempRecords);
                    }

                }
            })
    }


    searchAllRecords(modulename, filter, msg , pageNo, tempRecords = []){
        let self = this;
        return ZOHO.CRM.API.searchRecord({Entity:modulename,Type:"criteria",Query:filter,page:pageNo})
            .then(function(resp){
                if(resp.status!==204){
                    tempRecords = tempRecords.concat(resp.data);
                    $(msg).html("Fetching ("+ tempRecords.length+" records)");

                    if(resp.info.more_records===true){
                        return self.delay(800).then(()=> {
                            return self.searchAllRecords(modulename,filter, msg,++pageNo, tempRecords);
                        });
                    }
                    else{
                        return Promise.resolve(tempRecords);
                    }

                }else{
                    return Promise.resolve(tempRecords);
                }
            })
            .catch((err) => {
                return self.delay(60000).then(()=> {
                    return self.searchAllRecords(modulename,filter, pageNo, tempRecords);
                });
            });
    }

    createRecords(modulename, api_data, index = 0, tempRecords = [], trigger = []){
        let self = this;
        return ZOHO.CRM.API.insertRecord({Entity:modulename,APIData:api_data[index],Trigger:trigger}).then(function(data){
            tempRecords = tempRecords.concat(data.data);
            index++;
            if(api_data[index] === undefined){
                return Promise.resolve(tempRecords);
            }else{
                return self.createRecords(modulename, api_data, index, tempRecords);
            }
        });
    }

    delay(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

}

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fomantic-ui/2.8.7/semantic.min.css" integrity="sha512-g/MzOGVPy3OQ4ej1U+qe4D/xhLwUn5l5xL0Fa7gdC258ZWVJQGwsbIR47SWMpRxSPjD0tfu/xkilTy+Lhrl3xg==" crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset($location)}}/css/custom.css">
    <link rel="stylesheet" href="{{asset($location)}}/css/loader.css">
</head>
<body>


{{--<div id="start-loader" class="overlay" style="width: 100%">--}}
{{--    <div class=" container font-display" align="center" id="first-load">--}}
{{--        <h4 id="msg-load">Please wait while we load your records</h4>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="main ui container fluid" id="zero-page-view">
    <div class="html ui top attached segment">
        <div class="ui grid">
            <div class="sixteen wide column">

                <div class="ui form">
                    <div class="fields inline">
                        <div class="four wide field">
                            <label>Date Type: </label>
                            <select class="ui dropdown" id="slc-date-type">
                                <option value="Specific" selected>Specific Date</option>
                                <option value="Range">Date Range</option>
                            </select>
                        </div>

                        <div class="nine wide field" id="cont-specific">
                            <label>Specific Date: </label>
                            <div class="ui calendar input-date">
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="WIP Date" autocomplete="off" id="wip-end-date">
                                </div>
                            </div>
                        </div>
                        <div class="nine wide field" style="display: none;" id="cont-range">
                            <label>Date Range: </label>
                            <div class="ui calendar input-date" id="range-start">
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="Start Date" id="record-start-date" autocomplete="off" >
                                </div>
                            </div>
                            <div class="ui calendar input-date" id="range-end">
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="End Date"  id="record-end-date" autocomplete="off" >
                                </div>
                            </div>
                        </div>

                        <div class="three wide field right aligned">
                            <button class="ui icon button green basic" id="btn-generate-wip">
                                <i class="edit icon"></i>  Generate WIP
                            </button>
                        </div>
                    </div>
                </div>


{{--                <div class="ui action input">--}}
{{--                    <div class="ui calendar input-date">--}}
{{--                        <div class="ui input left icon">--}}
{{--                            <i class="calendar icon"></i>--}}
{{--                            <input type="text" placeholder="Please select WIP Date"  id="wip-date" autocomplete="off" >--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <button class="ui icon button green basic" id="btn-generate-wip">--}}
{{--                        <i class="edit icon"></i>  Generate WIP--}}
{{--                    </button>--}}
{{--                </div>--}}
            </div>

            <div class="sixteen wide column center aligned">
                <b id="span-selection"></b><span>. Please select a date and click on Generate WIP button to proceed on creating reports. <br> While on the process of creating records, please do not close the widget. Loading time is dependent on number of records selected. We will prompt a message once completed.</span>
            </div>
            <div class="sixteen wide column" id="tbl-summary-report">

            </div>

        </div>
    </div>
</div>


<div class="footer">
    {{$disclaimer}} @if($development_link) | Go to {!! $development_link !!}@endif
</div>



<script id="summary-list-script" type="text/x-handlebars-template">
    <table class="ui celled table">
        <thead>
        <tr>
            <th>#</th>
            <th>ID</th>
            <th>Name</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @{{#each this}}
        <tr class="@{{this}}">
            <td>@{{addOne @index}}</td>
            <td><a href="@{{getURL 'Deals' this}}" target="_blank">@{{this}}</a></td>
            <td class="td-deal-name">Pending</td>
            <td class="td-status">Pending</td>
        </tr>
        @{{/each}}
        </tbody>
    </table>

    <br><br>
</script>


</body>


<!-- js -->
<script src="https://live.zwidgets.com/js-sdk/1.0.5/ZohoEmbededAppSDK.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.js"></script>

<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.semanticui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fomantic-ui/2.8.7/semantic.min.js" integrity="sha512-1Nyd5H4Aad+OyvVfUOkO/jWPCrEvYIsQENdnVXt1+Jjc4NoJw28nyRdrpOCyFH4uvR3JmH/5WmfX1MJk2ZlhgQ==" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.6/handlebars.min.js"></script>

<script src="{{asset($location)}}/js/submain.js"></script>
<script src="{{asset($location)}}/js/mapping.js"></script>
<script src="{{asset($location)}}/js/handlebar_script.js"></script>
<script src="{{asset($location)}}/js/filter.js"></script>
<script src="{{asset($location)}}/js/client.js"></script>
<script src="{{asset($location)}}/js/main.js"></script>

</body>
</html>